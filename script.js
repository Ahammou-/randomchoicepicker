const tagsElmt = document.getElementById('tags');
const textarea = document.getElementById('textarea');

textarea.focus();

textarea.addEventListener('keyup', (e)=> {
    createTags(e.target.value);

    if (e.key === 'Enter') {
        setTimeout(() => {
            e.target.value = '';
        }, 10);

        randomSelect()
    }
});

const createTags = (input) => {
    const tags = input.split(',').filter(tag => tag.trim() !== '').map(tag => tag.trim());

    tagsElmt.innerHTML = '';
    
    tags.forEach(tag => {
        const tagElmt = document.createElement('span');
        tagElmt.classList.add('tag');
        tagElmt.innerText = tag;

        tagsElmt.appendChild(tagElmt);
    })
}

const randomSelect = () => {
    const times = 30;
    const interval = setInterval(() => {
        const randomTag = pickRandomTag();

        highlightTag(randomTag);

        setTimeout(() => {
            unHighlightTag(randomTag);
        }, 100);
    }, 100);

    setTimeout(() => {
        clearInterval(interval);

        setTimeout(() => {
            const randomTag = pickRandomTag();

            highlightTag(randomTag);
        }, 100);
    }, times * 100);
}

const pickRandomTag = () => {
    const tags = document.querySelectorAll('.tag');

    return tags[Math.floor(Math.random() * tags.length)];
}

const highlightTag = (tag) => {
    tag.classList.add('highlight');
}

const unHighlightTag = (tag) => {
    tag.classList.remove('highlight');
}